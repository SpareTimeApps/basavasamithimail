# README #

### What is this repository for? ###

This is a NodeJS service to send emails to the subcribers of the Basava Samithi of NZ association. 

### Technical details ###

The service uses ExpressJS, Body-Parser and NodeMailer for achieving the functionality. The script "src/mailer/index.js 
has the logic to send a simple email response. 

