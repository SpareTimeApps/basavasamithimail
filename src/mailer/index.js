import nodemailer from 'nodemailer'

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        type: 'OAuth2',
        user: 'kshivani1711@gmail.com',
        clientId: '269065493115-r2ci35d788upm619qrk7k8pvqfv0lipf.apps.googleusercontent.com',
        clientSecret: 'RYM_sooLTEqQwBs7X1of49Mi',
        refreshToken: '1/HJBLKd_cLxxcguM7SX7jVaZ4E8AcudN-uTYB1u5W6Ag',
        accessToken: 'ya29.GlsBBfbxfpaRfvJsVNpemLA7d6po0W0OKOcq2UXnCNMGZjDxSnAyvtn_5Y01dIJ4vUDiuQd2OfkNStWq5eNFNKU-gKljob22iCyr2Kn5ot19TPucdSpF3j0h-lVB',
        expires: 1484314697598
    }
});

transporter.set('oauth2_provision_cb', (user, renew, callback)=>{
    let accessToken = userTokens[user];
    if(!accessToken){
        return callback(new Error('Unknown user'));
    }else{
        return callback(null, accessToken);
    }
});

const send = ({ email, name, text }) => {
  const from = name && email ? `${name} <${email}>` : `${name || email}`
  const message = {
    from,
    to: 'kshivani1711@gmail.com',
    subject: `New message from ${from} to basavasamiti admin`,
    text,
    replyTo: from
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(message, (error, info) =>
      error ? reject(error) : resolve(info)
    )
  })
}

export default send
